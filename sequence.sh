#! /bin/sh

mount -o loop,offset=105906176 /image/mlxtof-zynq-3.4.img /mnt/

export PKG_CONFIG_PATH=/mnt/usr/lib/aarch64-linux-gnu/pkgconfig

/usr/bin/aarch64-linux-gnu-g++-7 -std=c++11 main.cpp --sysroot=/mnt -L/mnt/usr/lib/aarch64-linux-gnu/blas -lblas -L/mnt/usr/lib/aarch64-linux-gnu/lapack -llapack `pkg-config --cflags --libs opencv`
