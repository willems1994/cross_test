FROM ubuntu:19.04

RUN apt-get clean && apt-get update; \
    apt-get -y install gcc-7-aarch64-linux-gnu make git cmake g++-7-aarch64-linux-gnu build-essential vim pkg-config; \
    rm -rf /var/lib/apt/lists/*; \
    apt-get autoclean -qy
