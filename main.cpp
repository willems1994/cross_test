#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core/types_c.h>

#define NUM_CHANNELS 4

cv::Mat img[NUM_CHANNELS];
cv::Mat img_16bit[NUM_CHANNELS];
const char *img_filename[NUM_CHANNELS] = {
    "image-0.jpg",
    "image-1.jpg",
    "image-2.jpg",
    "image-3.jpg"
};

int main(int argc, char *argv[])
{
    int i = 0;
    (void) argc;
    (void) argv;

    /* Load test images into ram */
    for (i = 0; i < NUM_CHANNELS; i++) {
        img[i] = cv::imread(img_filename[i], cv::IMREAD_ANYDEPTH);
        if (!img[i].data) {
            fprintf(stderr, "img%d read error: %s\n", i, img_filename[i]);
            return -1;
        }
        img[i].convertTo(img_16bit[i], CV_16UC1);
        if (!img[i].data) {
            fprintf(stderr, "img%d convert error\n", i);
            return -1;
        }
    }

    std::cout << "finished!" << std::endl;

    return 0;
}
